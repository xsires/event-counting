package ru.xsires.event;


public interface EventCounter {

    void occur();

    long getEventCountForLastDay();

    long getEventCountForLastHour();

    long getEventCountForLastMinute();


}
