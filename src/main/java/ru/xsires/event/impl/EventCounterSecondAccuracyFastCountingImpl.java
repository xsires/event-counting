package ru.xsires.event.impl;

import ru.xsires.event.EventCounter;

import java.util.concurrent.atomic.LongAdder;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static ru.xsires.event.impl.TimeUnits.*;

/**
 * <p>
 * Реализация на основе circle array, время расчета количества событий O(1).
 * </p>
 * <p>
 * Инкремент O(1). Получение количества событий, или инкремент в каждую новую секунду блокирует структуру, время блокировки коррелирует со временем последнего обращения к ней,
 * чем меньше разница во времени с момента последнего обращения, тем меньше время блокировки.
 * </p>
 * <p>
 * Реализация предпочтительна когда расчет количества событий частый.
 * </p>
 */
public class EventCounterSecondAccuracyFastCountingImpl implements EventCounter {

    private final Lock lock = new ReentrantLock();
    /**
     * circle array в котором хронятся колличество вызовов {@link #occur()},
     * нужен для номрализации счетчиков <code>lastMinuteCounter, lastHourCounter, lastDayCounter </code>
     */
    private final LongAdder[] hitCountersBySecond = new LongAdder[SECOND_IN_DAY];
    /**
     * Счетчики вызовов, за последнюю минуту, секунду, день, при простои нормализуются в {@link #resetCountersIfNecessary(long)}
     */
    private final LongAdder lastMinuteCounter = new LongAdder();
    private final LongAdder lastHourCounter = new LongAdder();
    private final LongAdder lastDayCounter = new LongAdder();
    /**
     * Время последнего побращения к  {@link EventCounter}
     */
    private volatile long lastHitSecond;

    public EventCounterSecondAccuracyFastCountingImpl() {
        this.lastHitSecond = getCurrentSecond();
        for (int i = 0; i < hitCountersBySecond.length; i++) {
            hitCountersBySecond[i] = new LongAdder();
        }

    }

    @Override
    public void occur() {
        long now = getCurrentSecond();
        resetCountersIfNecessary(now);

        hitCountersBySecond[(int) (now % SECOND_IN_DAY)].increment();
        lastMinuteCounter.increment();
        lastHourCounter.increment();
        lastDayCounter.increment();

    }

    @Override
    public long getEventCountForLastDay() {
        resetCountersIfNecessary(getCurrentSecond());
        return lastDayCounter.longValue();
    }

    @Override
    public long getEventCountForLastHour() {
        resetCountersIfNecessary(getCurrentSecond());
        return lastHourCounter.longValue();
    }

    @Override
    public long getEventCountForLastMinute() {
        resetCountersIfNecessary(getCurrentSecond());
        return lastMinuteCounter.longValue();
    }

    /**
     * Будет срабатывать в худшем случае раз в секунду,
     * блокируя потоки выполнения, уменьшает счеткики событий на колличество вызовов {@link #occur()} между
     * <code>lastHitSecond<code/> и <code>now</code>
     *
     * @param now - Текущая секунда
     */
    private void resetCountersIfNecessary(long now) {
        if (now > lastHitSecond) {
            lock.lock();
            try {
                if (now > lastHitSecond) {
                    //если последнее срабатывание было сутки назад, ничего перерассчитывать на недо, просто сбросим таймеры
                    if ((now - lastHitSecond) >= SECOND_IN_DAY) {
                        lastMinuteCounter.reset();
                        lastHourCounter.reset();
                        lastDayCounter.reset();
                        for (LongAdder longAdder : hitCountersBySecond) {
                            longAdder.reset();
                        }
                    } else {
                        // на каждой итерации цикла надо уменьшить счетчики вызовов
                        for (long i = lastHitSecond + 1; i <= now; i++) {
                            // всю информацию о вызовах которые были SECOND_IN_DAY назад надо сбросить
                            // счетчик по дням уменьшить на эту цифру
                            int dayBucket = (int) (i % SECOND_IN_DAY);
                            lastDayCounter.add(-hitCountersBySecond[dayBucket].sumThenReset());

                            // уменьшим счётчик lastHourCounter на значение о вызовах которые были SECOND_IN_HOUR назад
                            int hourBucket = (int) ((i - SECOND_IN_HOUR) % SECOND_IN_DAY);
                            lastHourCounter.add(-hitCountersBySecond[hourBucket].longValue());

                            int minuteBucket = (int) ((i - SECOND_IN_MINUTE) % SECOND_IN_DAY);
                            lastMinuteCounter.add(-hitCountersBySecond[minuteBucket].longValue());
                        }
                    }
                    // запомним секунду, в которой нормализовали счетчики
                    lastHitSecond = now;
                }
            } finally {

                lock.unlock();
            }


        }

    }

    /**
     * @return Текущая секунда
     */
    long getCurrentSecond() {
        return System.currentTimeMillis() / 1000;
    }

    @Override
    public String toString() {
        return "EventCounterSecondAccuracyFastCountingImpl{" +
                "lastMinuteCounter=" + lastMinuteCounter +
                ", lastHourCounter=" + lastHourCounter +
                ", lastDayCounter=" + lastDayCounter +
                ", lastHitSecond=" + lastHitSecond +
                '}';
    }
}
