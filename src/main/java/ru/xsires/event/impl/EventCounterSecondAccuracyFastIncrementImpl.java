package ru.xsires.event.impl;

import ru.xsires.event.EventCounter;

import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.LongAdder;

import static ru.xsires.event.impl.TimeUnits.*;


/**
 * Реализация на основе {@link ConcurrentHashMap}, время расчета количества событий
 * O({@link EventCounterSecondAccuracyFastIncrementImpl#MAX_SIZE}),
 * Инкремент почти всегда O(1).
 * <p>
 * Когда количество "элементов" переваливает за
 * {@link EventCounterSecondAccuracyFastIncrementImpl#MAX_SIZE}
 * случайный поток, который хотел получить количество событий или сделать инкремент
 * будет чистить не нужные элементы. Чистка может происходить не чаще чем раз в 59 мин при условии что
 * каждую секунду минимум один раз происходит инкремент.
 * </p>
 * <p>
 * "элемент" это MapEntry, где ключ - секунда, значение - количество событий произошедшее в эту секунду.
 * </p>
 * <p>
 * Реализация предпочтительна когда расчет количества событий редкий.
 * </p>
 */
public class EventCounterSecondAccuracyFastIncrementImpl implements EventCounter {

    /**
     * Размер после достижения которого надо чистить <code>hitCountersPerSecond</code>
     */
    static final int MAX_SIZE = SECOND_IN_DAY + SECOND_IN_HOUR;

    /**
     * Сколько надо оставить элементов после очередной очискти, это не число элементов, а время от момента очистки.
     */
    private static final int USEFUL_SECOND_SHOULD_LEFT = SECOND_IN_DAY + SECOND_IN_MINUTE;


    /**
     * <p>
     * <code>key</code> - секунда, <code>value</code>  - колличество вызовов в секунду <code>key</code>
     * </p>
     * Действительный размер 2^17
     */
    final ConcurrentHashMap<Long, LongAdder> hitCountersPerSecond = new ConcurrentHashMap<>(MAX_SIZE);
    /**
     * Размер <code>hitCountersPerSecond</code>
     */
    final AtomicInteger hitCountersPerSecondSize = new AtomicInteger(0);
    /**
     * флаг чтобы параллельно не было очистки
     */
    private final AtomicBoolean isNowClean = new AtomicBoolean(false);

    public void occur() {
        long now = getCurrentSecond();
        LongAdder longAdder = hitCountersPerSecond.get(now);
        if (longAdder == null) {
            // если еще в эту секунду не присхоидло событе создадим счетчик
            LongAdder newLongAdder = new LongAdder();
            longAdder = hitCountersPerSecond.putIfAbsent(now, newLongAdder);
            if (longAdder == null) {
                longAdder = newLongAdder;
                hitCountersPerSecondSize.incrementAndGet();
            }
        }
        longAdder.increment();
        removeGarbageIfNecessary(now);
    }

    private void removeGarbageIfNecessary(long now) {
        // если лишних записей слишком много, пора удалять ненужные счетчики
        if (hitCountersPerSecondSize.get() >= MAX_SIZE) {
            if (isNowClean.compareAndSet(false, true)) {
                try {
                    if (hitCountersPerSecondSize.get() >= MAX_SIZE) {
                        //от "сейчас" отнимим колличество секунд в дне + 1 мин, и удалим все элементы которые были добавлены до этого времени
                        long removeFrom = now - USEFUL_SECOND_SHOULD_LEFT;
                        for (Iterator<Map.Entry<Long, LongAdder>> iterator = hitCountersPerSecond.entrySet().iterator(); iterator.hasNext(); ) {
                            Map.Entry<Long, LongAdder> next = iterator.next();
                            if (removeFrom >= next.getKey()) {
                                iterator.remove();
                                hitCountersPerSecondSize.decrementAndGet();
                            }
                        }
                    }
                } finally {
                    isNowClean.set(false);
                }
            }
        }

    }


    @Override
    public long getEventCountForLastDay() {
        long currentSecond = getCurrentSecond();
        removeGarbageIfNecessary(currentSecond);
        return countEvents(currentSecond - SECOND_IN_DAY, currentSecond);
    }

    @Override
    public long getEventCountForLastHour() {
        long currentSecond = getCurrentSecond();
        removeGarbageIfNecessary(currentSecond);
        return countEvents(currentSecond - SECOND_IN_HOUR, currentSecond);
    }

    @Override
    public long getEventCountForLastMinute() {
        long currentSecond = getCurrentSecond();
        removeGarbageIfNecessary(currentSecond);
        return countEvents(currentSecond - SECOND_IN_MINUTE, currentSecond);
    }

    /**
     * Посчитаем колличество событий за вермя с <code>from</code> до <code>to</code>,
     * скорость просчета на прямую зависит от размера <code>hitCountersPerSecond</code>
     *
     * @param from исключительно
     * @param to включительно
     * @return колличество событий
     */

    private long countEvents(final long from, final long to) {
        long result = 0;
        for (Map.Entry<Long, LongAdder> next : hitCountersPerSecond.entrySet()) {
            long key = next.getKey();
            if (from < key && key <= to) {
                result += next.getValue().longValue();
            }

        }
        return result;
    }


    long getCurrentSecond() {
        return System.currentTimeMillis() / 1000;
    }

}