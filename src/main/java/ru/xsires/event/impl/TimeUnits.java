package ru.xsires.event.impl;


public class TimeUnits {

    public static final int SECOND_IN_DAY = 24 * 60 * 60;
    public static final int SECOND_IN_HOUR = 60 * 60;
    public static final int SECOND_IN_MINUTE = 60;
}
