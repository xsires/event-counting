package ru.xsires.event.impl;

public class EventCounterSecondAccuracyFastCountingImplTest extends EventCounterTest {

	@Override
	protected EventCounterManagementMoment createEventCounter() {
		return new EventCounterSecondAccuracyFastCountingImplTestMock();
	}

	public static class EventCounterSecondAccuracyFastCountingImplTestMock extends EventCounterSecondAccuracyFastCountingImpl
			implements EventCounterManagementMoment {

		volatile long currentSecond = System.currentTimeMillis() / 1000;
		volatile boolean isTimeManagementDisabled = false;

		@Override
		public long getCurrentSecond() {
			if (isTimeManagementDisabled){
				return System.currentTimeMillis()/1000;
			}
			return currentSecond;
		}

		public void setCurrentSecond(long value) {
			currentSecond = value;
		}

		@Override
		public void disableTimeManagement() {
			isTimeManagementDisabled = true;

		}
	}

}