package ru.xsires.event.impl;

import org.junit.Assert;
import org.junit.Test;

import static ru.xsires.event.impl.TimeUnits.SECOND_IN_DAY;

public class EventCounterSecondAccuracyFastIncrementImplTest extends EventCounterTest {

	@Override
	protected EventCounterManagementMoment createEventCounter() {
		return new EventCounterSecondAccuracyFastIncrementImplTestMock();
	}

	/**
	 * Проверяем что ненужные счетчики со временем умирают
	 */
	@Test
	public void noLeak() {
		EventCounterSecondAccuracyFastIncrementImpl ec = (EventCounterSecondAccuracyFastIncrementImpl) eventCounter;

		for (int i = 0; i < SECOND_IN_DAY * 7; i++) {
			eventCounter.occur();
			shiftCurrentTime(1);
			if (i % 1000 == 0) {
				Assert.assertTrue(
						ec.hitCountersPerSecond.size() <= EventCounterSecondAccuracyFastIncrementImpl.MAX_SIZE);
				Assert.assertTrue(
						ec.hitCountersPerSecondSize.get() <= EventCounterSecondAccuracyFastIncrementImpl.MAX_SIZE);
			}
		}
	}

	public static class EventCounterSecondAccuracyFastIncrementImplTestMock extends EventCounterSecondAccuracyFastIncrementImpl
			implements EventCounterManagementMoment {

		volatile boolean isTimeManagementDisabled = false;
		volatile long currentSecond = System.currentTimeMillis() / 1000;

		@Override
		public long getCurrentSecond() {
			if (isTimeManagementDisabled) {
				return System.currentTimeMillis() / 1000;
			}
			return currentSecond;
		}

		public void setCurrentSecond(long value) {
			currentSecond = value;
		}

		@Override
		public void disableTimeManagement() {
			isTimeManagementDisabled = true;

		}
	}

}
