package ru.xsires.event.impl;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import ru.xsires.event.EventCounter;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static ru.xsires.event.impl.TimeUnits.*;

public abstract class EventCounterTest {


    EventCounterManagementMoment eventCounter;


    void shiftCurrentTime(long amountSecond) {
        eventCounter.setCurrentSecond(eventCounter.getCurrentSecond() + amountSecond);
    }

    protected abstract EventCounterManagementMoment createEventCounter();

    @Before
    public void before() {
        eventCounter = createEventCounter();
    }


    /**
     * Проверяем что после простоя в минуту, час, день счетчики показывают верные значения
     */
    @Test
    public void getEventCountAfterDelay() {
        eventCounter.occur();
        assertCountHits(1, 1, 1, eventCounter);

        shiftCurrentTime(SECOND_IN_MINUTE);
        assertCountHits(0, 1, 1, eventCounter);

        shiftCurrentTime(SECOND_IN_HOUR);
        assertCountHits(0, 0, 1, eventCounter);

        shiftCurrentTime(SECOND_IN_DAY);
        assertCountHits(0, 0, 0, eventCounter);

        eventCounter.occur();
        shiftCurrentTime(SECOND_IN_MINUTE - 1);
        assertCountHits(1, 1, 1, eventCounter);

        shiftCurrentTime(1);
        assertCountHits(0, 1, 1, eventCounter);
    }


    /**
     * происходит, событие, увеличиваем время на 1 сек,
     * мин, час, день,
     * делаем 10 вызовов по 10 раз конкурентно, проверяем что ничего не потеряли
     */
    @Test
    public void notLostHitsOnNextSecond() {
        assertCountHits(0, 0, 0, eventCounter);
        make100HitConcurrently(eventCounter);
        assertCountHits(100, 100, 100, eventCounter);

        shiftCurrentTime(SECOND_IN_MINUTE);
        make100HitConcurrently(eventCounter);
        assertCountHits(100, 200, 200, eventCounter);

        shiftCurrentTime(SECOND_IN_HOUR);

        make100HitConcurrently(eventCounter);
        assertCountHits(100, 100, 300, eventCounter);

        shiftCurrentTime(SECOND_IN_DAY);
        make100HitConcurrently(eventCounter);
        assertCountHits(100, 100, 100, eventCounter);

    }

    /**
     * в течении SECOND_IN_DAY + 1, раз в секунду срабатывает событие,
     * проверяем что количество событий за последний час, день, минут, верное
     */
    @Test
    public void correctCounting() {
        assertCountHits(0, 0, 0, eventCounter);

        for (int i = 1; i <= SECOND_IN_DAY * 2 + 1; i++) {
            shiftCurrentTime(1);
            eventCounter.occur();
            if (i % 1000 == 0) {
                long expectedMinuteHits = i > SECOND_IN_MINUTE ? SECOND_IN_MINUTE : i;
                long expectedHourHits = i > SECOND_IN_HOUR ? SECOND_IN_HOUR : i;
                long expectedDayHits = i > SECOND_IN_DAY ? SECOND_IN_DAY : i;
                assertCountHits(expectedMinuteHits, expectedHourHits, expectedDayHits, eventCounter);
            }

        }

        shiftCurrentTime(SECOND_IN_DAY);
        assertCountHits(0, 0, 0, eventCounter);
    }


	private void make100HitConcurrently(final EventCounter eventCounter) {

        CyclicBarrier cyclicBarrier = new CyclicBarrier(10);
        final CountDownLatch threadDoneLock = new CountDownLatch(10);

        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                try {
                    cyclicBarrier.await();

                    for (int j = 0; j < 10; j++) {
                        eventCounter.occur();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    threadDoneLock.countDown();
                }
            }).start();
        }

        try {
            threadDoneLock.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    private void assertCountHits(long minute, long hour, long day, EventCounter eventCounter) {
        assertEquals(minute, eventCounter.getEventCountForLastMinute());
        assertEquals(hour, eventCounter.getEventCountForLastHour());
        assertEquals(day, eventCounter.getEventCountForLastDay());
    }

	/**
	 * {@link EventCounter} с возможностью управлять сколько секунд "сейчас"
	 */
    public interface EventCounterManagementMoment extends EventCounter {

        long getCurrentSecond();

        void setCurrentSecond(long now);

		/**
		 * Переводим в real-time режим, {@link #getCurrentSecond()} будет возвращать текущее время
		 */
		void disableTimeManagement();

    }


}
