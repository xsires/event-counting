package ru.xsires.event.impl.benchmark;

import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import org.openjdk.jmh.runner.options.TimeValue;
import ru.xsires.event.EventCounter;
import ru.xsires.event.impl.EventCounterSecondAccuracyFastCountingImplTest;
import ru.xsires.event.impl.EventCounterSecondAccuracyFastIncrementImplTest;
import ru.xsires.event.impl.EventCounterTest;
import ru.xsires.event.impl.TimeUnits;

import java.util.Random;
import java.util.concurrent.TimeUnit;

abstract class BenchmarkAbstract {

    EventCounter fastInc = null;
	EventCounter fastCount = null;

    static Options build(Class<? extends BenchmarkAbstract> clazz) {
        return new OptionsBuilder()
                .include(clazz.getSimpleName())
                .warmupIterations(1)
                .measurementIterations(6)
                .measurementTime(new TimeValue(10, TimeUnit.SECONDS))
                .forks(1)
                .build();
    }


    void prepare() {
        // перед началом заполним счетчики, как будто они работали 2 дня подрят
        EventCounterTest.EventCounterManagementMoment fastIncMock = new EventCounterSecondAccuracyFastIncrementImplTest.EventCounterSecondAccuracyFastIncrementImplTestMock();
        EventCounterTest.EventCounterManagementMoment fastCountMock = new EventCounterSecondAccuracyFastCountingImplTest.EventCounterSecondAccuracyFastCountingImplTestMock();

        int loopCycles = TimeUnits.SECOND_IN_DAY * 2;
        long currentTime = System.currentTimeMillis() / 1000 - loopCycles - 1;

        // отмотаем "сейчас" на 2 дня назад, что бы заполнить таймеры
        fastIncMock.setCurrentSecond(currentTime);
        fastCountMock.setCurrentSecond(currentTime);

        Random random = new Random();
        for (int i = 0; i < loopCycles; i++) {
            fastIncMock.setCurrentSecond(fastIncMock.getCurrentSecond() + 1);
            fastCountMock.setCurrentSecond(fastCountMock.getCurrentSecond() + 1);
            // сделаем от 0 до 10 вызовов
            for (int k = 0; k < random.nextInt(10); k++) {
                fastIncMock.occur();
                fastCountMock.occur();
            }
        }

        fastIncMock.disableTimeManagement();
        fastCountMock.disableTimeManagement();

        fastInc = fastIncMock;
        fastCount = fastCountMock;
    }
}
