package ru.xsires.event.impl.benchmark;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Group;
import org.openjdk.jmh.annotations.GroupThreads;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;

import java.util.concurrent.TimeUnit;

/**
 * <p>
 * Сетчики работают 2 дня, 10 потоков получают количество событий за последний час
 * </p>
 * <table>
 * <tr>
 * <th>Benchmark</th>
 * <th>Mode</th>
 * <th>Cnt</th>
 * <th>Score</th>
 * <th>Error</th>
 * <th>Units</th>
 * </tr>
 * <tr>
 * <td>CountingBenchmark.fastCount</td>
 * <td>thrpt</td>
 * <td>6</td>
 * <td>119072,454</td>
 * <td>±5967,258</td>
 * <td>ops/ms</td>
 * </tr>
 * <tr>
 * <td>CountingBenchmark.fastInc</td>
 * <td>thrpt</td>
 * <td>6</td>
 * <td>0,903</td>
 * <td>±0,119</td>
 * <td>ops/ms</td>
 * </tr>
 * </table>
 */
@State(Scope.Group)
@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
public class CountingBenchmark extends BenchmarkAbstract {

	public static void main(String[] args) throws RunnerException {
		new Runner(build(CountingBenchmark.class)).run();
	}

	@Setup
	public void setup() {
		super.prepare();
	}

	@Benchmark
	@Group("fastInc")
	@GroupThreads(10)
	public long fastInc() {
		return fastInc.getEventCountForLastHour();
	}

	@Benchmark
	@Group("fastCount")
	@GroupThreads(10)
	public long fastCount() {
		return fastCount.getEventCountForLastHour();
	}
}