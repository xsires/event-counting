package ru.xsires.event.impl.benchmark;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Group;
import org.openjdk.jmh.annotations.GroupThreads;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;

import java.util.concurrent.TimeUnit;

/**
 * <p>
 * Сетчики работают 2 дня, затем 100 потоков увеличивают счетчики
 * </p>
 * <table>
 * <tr>
 * <th>Benchmark</th>
 * <th>Mode</th>
 * <th>Cnt</th>
 * <th>Score</th>
 * <th>Error</th>
 * <th>Units</th>
 * </tr>
 * <tr>
 * <td>IncrementBenchmark.fastCount</td>
 * <td>thrpt</td>
 * <td>6</td>
 * <td>44521,360</td>
 * <td>±879,340</td>
 * <td>ops/ms</td>
 * </tr>
 * <tr>
 * <td>IncrementBenchmark.fastInc</td>
 * <td>thrpt</td>
 * <td>6</td>
 * <td>73357,575</td>
 * <td>±1134,442</td>
 * <td>ops/ms</td>
 * </tr>
 * </table>
 */
@State(Scope.Group)
@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
public class IncrementBenchmark extends BenchmarkAbstract {

	public static void main(String[] args) throws RunnerException {
		new Runner(build(IncrementBenchmark.class)).run();
	}

	@Setup
	public void setup() {
		super.prepare();
	}

	@Benchmark
	@Group("fastInc")
	@GroupThreads(100)
	public void fastInc() {
		fastInc.occur();
	}

	@Benchmark
	@Group("fastCount")
	@GroupThreads(100)
	public void fastCount() {
		fastCount.occur();
	}
}
